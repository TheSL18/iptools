#!/bin/bash

#Datos del desarrolador
#kevin david muñoz moreno
#tel 3025685220
#email david.munozm@protonmail.com

#Datos de version
PROGNAME=IPTools
NAMEPROPER=IPTools
VERSION=1.0.0

clear
#Funcion de salida
function ctrl_c(){
    echo -e "\n[!] Saliendo...\n"
    exit 1
}

#Orden ctrl+c
trap ctrl_c INT

#Banner

echo "██╗██████╗░████████╗░█████╗░░█████╗░██╗░░░░░░██████╗"
echo "██║██╔══██╗╚══██╔══╝██╔══██╗██╔══██╗██║░░░░░██╔════╝"
echo "██║██████╔╝░░░██║░░░██║░░██║██║░░██║██║░░░░░╚█████╗░"
echo "██║██╔═══╝░░░░██║░░░██║░░██║██║░░██║██║░░░░░░╚═══██╗"
echo "██║██║░░░░░░░░██║░░░╚█████╔╝╚█████╔╝███████╗██████╔╝"
echo "╚═╝╚═╝░░░░░░░░╚═╝░░░░╚════╝░░╚════╝░╚══════╝╚═════╝░"
sleep 1
echo ""
echo "█▄▄ █▄█   █▀▄▀█ █▀█ ░ █░█ ▄▀█ █▀▀ █▄▀ █▀▀ █▀█"
echo "█▄█ ░█░   █░▀░█ █▀▄ ▄ █▀█ █▀█ █▄▄ █░█ ██▄ █▀▄"
echo ""
sleep 1

#Definicion de la herramienta
echo "Herramienta de automatizacion para la obtencion de tiempo de caida para las aplicaciones de seguros bolivar"
echo "[+] Presione 1 para usar api ipinfo.io "
echo "[+] Presione 2 para usar api iplist "
echo "[+] Presione 3 para obtener informacion relevante del servidor "
echo "Ingresa una opcion -> "

#Lee la variable ingresada para acceder al menú
read opcion

#Usa case para encontrar un match en el menu de opciones y proceder con la accion
case $opcion in

#Opciones
1)
echo "[+] Ingresa la direccion IP o URL a escanear (ejemplo 8.8.8.8): "
read ip
curl ipinfo.io/"$ip"?token=9b401e36c88cc6
sleep 1
echo ""
echo "Gracias por utilizar esta herramienta"
;;

2)
echo "[+] Ingresa la direccion IP o URL a escanear (ejemplo 8.8.8.8 o google.com): "
read ip
curl -s https://iplist.cc/api/"$ip"
sleep 1
echo ""
echo "Gracias por utilzar esta herramienta"
;;

3)
echo "[+] Ingresa la direccion IP o URL a escanear (ejemplo 8.8.8.8 o google.com): "
read ip
curl -s """https://ipvigilante.com/json/'$ip'/full"""
sleep 1
echo ""
echo "Gracias por utilzar esta herramienta"
;;

esac

#Querido programador:
#Cuando escribi este codigo, solo Odin y yo
#sabiamos que funcionaba.
#Ahora, !sólo Odin lo sabe¡
#
#Asi que si está tratando de "optimizar"
#este codigo y fracasa (Seguramente),
#por favor incremente el siguiente contador
#como una advertencia
#para el siguiente colega
#
#Total_horas_perdidas_aqui = 3